package net.iescierva.ajfp.p0407distancias;

public class GeoException extends Exception {
    GeoException(String e) {
        super(e);
    }
};