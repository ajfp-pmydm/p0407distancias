package net.iescierva.ajfp.p0407distancias;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import java.util.Locale;
import net.iescierva.ajfp.p0407distancias.databinding.ActivityMainBinding;

//Versión alternativa del ejercicio hecho con DataBinding ya que hay muchos componentes
public class MainActivity extends AppCompatActivity {

    public ActivityMainBinding dataBind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataBind = DataBindingUtil.setContentView(this, R.layout.activity_main);

        dataBind.btnZona.setOnClickListener(v -> {
            try {
                GeoPunto p1=new GeoPunto(
                        dataBind.p1latG.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1latG.getText().toString()),
                        dataBind.p1latM.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1latM.getText().toString()),
                        dataBind.p1latS.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1latS.getText().toString()),
                        dataBind.p1lonG.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1lonG.getText().toString()),
                        dataBind.p1lonM.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1lonM.getText().toString()),
                        dataBind.p1lonS.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1lonS.getText().toString())
                );

                if (dataBind.p1LatMinus.isChecked()) p1.setLatitud((-1)* p1.getLatitud());

                GeoPunto p2=new GeoPunto(
                        dataBind.p2latG.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2latG.getText().toString()),
                        dataBind.p2latM.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2latM.getText().toString()),
                        dataBind.p2latS.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2latS.getText().toString()),
                        dataBind.p2lonG.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2lonG.getText().toString()),
                        dataBind.p2lonM.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2lonM.getText().toString()),
                        dataBind.p2lonS.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2lonS.getText().toString())
                );
                if (dataBind.p2LatMinus.isChecked()) p2.setLatitud((-1)*p2.getLatitud());

                double lat1 = p1.getLatitud();
                double lat2 = p2.getLatitud();
                String mensaje = "El punto 1 se encuentra en " + calculaMensaje(lat1) + "\n" +
                                            "El punto 2 se encuentra en " + calculaMensaje(lat2);

                new AlertDialog.Builder(this)
                        .setTitle("Zona del lugar")
                        .setMessage(mensaje)
                        .setPositiveButton("Aceptar", null)
                        .show();

            } catch (GeoException e) {
                e.printStackTrace();
            }

        });

        dataBind.btnBorrar.setOnClickListener(v -> {
            dataBind.p1LatMinus.setChecked(false);
            dataBind.p1LonMinus.setChecked(false);
            dataBind.p1latG.setText("");
            dataBind.p1latM.setText("");
            dataBind.p1latS.setText("");
            dataBind.p1lonG.setText("");
            dataBind.p1lonM.setText("");
            dataBind.p1lonS.setText("");

            dataBind.p2LatMinus.setChecked(false);
            dataBind.p2LonMinus.setChecked(false);
            dataBind.p2latG.setText("");
            dataBind.p2latM.setText("");
            dataBind.p2latS.setText("");
            dataBind.p2lonG.setText("");
            dataBind.p2lonM.setText("");
            dataBind.p2lonS.setText("");
        });

        dataBind.btnCalcular.setOnClickListener(view -> {
            try {
                GeoPunto p1=new GeoPunto(
                        dataBind.p1latG.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1latG.getText().toString()),
                        dataBind.p1latM.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1latM.getText().toString()),
                        dataBind.p1latS.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1latS.getText().toString()),
                        dataBind.p1lonG.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1lonG.getText().toString()),
                        dataBind.p1lonM.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1lonM.getText().toString()),
                        dataBind.p1lonS.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p1lonS.getText().toString())
                );
                if (dataBind.p1LatMinus.isChecked()) p1.setLatitud((-1)* p1.getLatitud());
                if (dataBind.p1LonMinus.isChecked()) p1.setLongitud((-1)* p1.getLongitud());

                GeoPunto p2=new GeoPunto(
                        dataBind.p2latG.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2latG.getText().toString()),
                        dataBind.p2latM.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2latM.getText().toString()),
                        dataBind.p2latS.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2latS.getText().toString()),
                        dataBind.p2lonG.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2lonG.getText().toString()),
                        dataBind.p2lonM.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2lonM.getText().toString()),
                        dataBind.p2lonS.getText().toString().isEmpty()? 0: Double.parseDouble(dataBind.p2lonS.getText().toString())
                );
                if (dataBind.p2LatMinus.isChecked()) p2.setLatitud((-1)*p2.getLatitud());
                if (dataBind.p2LonMinus.isChecked()) p2.setLongitud((-1)*p2.getLongitud());

                /* Creamos un Geopunto con la latitud límite y lo comparamos con la latitud positiva y negativa de ambos puntos, porque así
                sólo tenemos que comparar el double que devuelve getLatitud con los tres campos de latitud, lo construimos sin
                asignar longitud porque no lo necesitamos */

                GeoPunto puntoPolar = new GeoPunto(66, 33, 46, 0, 0, 0);
                double latitudPolar = puntoPolar.getLatitud();
                if (latitudPolar >= p1.getLatitud() ||
                    latitudPolar <= - p1.getLatitud() ||
                    latitudPolar >= p2.getLatitud() ||
                    latitudPolar <= - p2.getLatitud()) {
                    Toast.makeText(this, "Error, ese punto se encuentra demasiado cerca de los polos.", Toast.LENGTH_SHORT).show();
                    return;
                }

                double result = p1.distancia(p2)/1000;
                Locale l=Locale.getDefault();
                dataBind.distancia.setText(String.format(l,"%10.2f", result)+" km");
            }
            catch (Exception e) {
               /* ejemplo de flag de errores
               if (dataBind.p1LatG.getText().toString().matches(""))
                      dataBind.p1LatG.setError(getResources().getString(R.string.error_valor_incorrecto));
               */
                dataBind.distancia.setText(getResources().getString(R.string.error_generico));
            }
        });
    }

    private double calculaLatitud(double latG, double latM, double latS){
        return latG + latM / 60D + latS / 3600D;
    }

    private String calculaMensaje(double latitud){
        String mensaje = "";
        if (latitud == 90){
            mensaje = "El polo norte";
        } else if ( latitud == -90){
            mensaje = "El polo sur";
        } else if ( latitud == calculaLatitud(23, 26, 14)){
            mensaje = "El trópico de Cáncer";
        } else if ( latitud == calculaLatitud(23, 26, 14) * -1){
            mensaje = "El trópico de Capricornio";
        } else if ( latitud == calculaLatitud(66, 33, 46)) {
            mensaje = "El círculo polar Ártico";
        } else if ( latitud ==calculaLatitud(66, 33, 46) * -1){
            mensaje = "El círculo polar Antártico";
        } else if ( latitud == 0){
            mensaje = "El Ecuador";
        }
        return mensaje;
    }
}